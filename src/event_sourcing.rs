#[derive(Debug)]
pub enum ErrorType {
    ValidationError(String),
    CommandError(String),
}

pub struct Error {
    pub kind: ErrorType,
}

pub trait Aggregate {
    type Event;
    type State;
    type Command;

    fn version(&self) -> u64;
    fn apply(state: &Self::State, event: &Self::Event) -> Result<Self::State, Error>;
    fn handle_command(state: &Self::State, cmd: &Self::Command) -> Result<Vec<Self::Event>, Error>;
}