mod event_sourcing;

use event_sourcing::*;
use uuid::Uuid;

type Price = u32;

enum ProductCommand {
    CreateProduct { product_id: Uuid, price: Price, title: String },
    ChangePrice { product_id: Uuid, price: Price },
    ChangeTitle { product_id: Uuid, title: String },
}

enum ProductEvent {
    ProductCreated { id: Uuid, product_id: Uuid },
    PriceChanged { id: Uuid, product_id: Uuid, price: Price },
    TitleChanged { id: Uuid, product_id: Uuid, title: String }
}

struct Product;

struct ProductState {
    id: Uuid,
    price: Price,
    title: String,
}

impl Aggregate for Product {
    type Event = ProductEvent;
    type State = ProductState;
    type Command = ProductCommand;

    fn version(&self) -> u64 {
        unimplemented!()
    }

    fn apply(state: &Self::State, event: &Self::Event) -> Result<Self::State, Error> {
        match event {
            ProductEvent::ProductCreated { id: _, product_id } => Ok(ProductState { id: *product_id, title: state.title.clone(), .. *state }),
            ProductEvent::PriceChanged { id: _, product_id: _, price } => Ok(ProductState { price: *price, title: state.title.clone(), .. *state }),
            ProductEvent::TitleChanged { id: _, product_id: _, title } => Ok(ProductState { title: title.clone(), ..*state }),
        }
    }

    fn handle_command(state: &Self::State, cmd: &Self::Command) -> Result<Vec<Self::Event>, Error> {
        match cmd {
            ProductCommand::CreateProduct { product_id: _, price: _, title: _ } => Product::handle_create_product(state, cmd),
            ProductCommand::ChangePrice { product_id: _, price: _} => Product::handle_change_price(state, cmd),
            ProductCommand::ChangeTitle { product_id: _, title: _ } => Product::handle_change_title(state, cmd),
        }
    }
}

impl Product {
    fn handle_create_product(state: &ProductState, command: &ProductCommand) -> Result<Vec<ProductEvent>, Error> {
        match command {
            ProductCommand::CreateProduct { product_id, price, title } => {
                if !Uuid::is_nil(&state.id) {
                    return Err(Error { kind: ErrorType::ValidationError("Product does exist".to_string()) });
                }

                Ok(vec![
                    ProductEvent::ProductCreated { id: Uuid::new_v4(), product_id: *product_id },
                    ProductEvent::PriceChanged { id: Uuid::new_v4(), product_id: *product_id, price: *price },
                    ProductEvent::TitleChanged { id: Uuid::new_v4(), product_id: *product_id, title: title.clone() },
                ])
            },
            _ => Err(Error { kind: ErrorType::CommandError("CreateProduct expected".to_string()) })
        }
    }

    fn handle_change_price(_state: &ProductState, command: &ProductCommand) -> Result<Vec<ProductEvent>, Error> {
        match command {
            ProductCommand::ChangePrice { product_id, price } => {
                Ok(vec![ProductEvent::PriceChanged { id: Uuid::new_v4(), product_id: *product_id, price: *price }])
            },
            _ => Err(Error { kind: ErrorType::CommandError("ChangePrice expected".to_string()) })
        }
    }

    fn handle_change_title(_state: &ProductState, command: &ProductCommand) -> Result<Vec<ProductEvent>, Error> {
        match command {
            ProductCommand::ChangeTitle { product_id, title } => {
                Ok(vec![ProductEvent::TitleChanged { id: Uuid::new_v4(), product_id: *product_id, title: title.to_string() }])
            },
            _ => Err(Error { kind: ErrorType::CommandError("ChangeTitle expected".to_string()) })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use uuid::Uuid;

    #[test]
    fn handling_a_create_product_event_returns_events() {
        let expected_product_id = Uuid::new_v4();
        let events = Product::handle_command(
            &ProductState { id: Uuid::nil(), price: 0, title: "".to_string() },
            &ProductCommand::CreateProduct { product_id: expected_product_id, price: 120, title: "Bob".to_string() },
        );

        match events {
            Ok(events) => {
                assert_eq!(events.len(), 3);
                match &events[0] {
                    ProductEvent::ProductCreated { id: _, product_id } => assert_eq!(
                        product_id.to_string(), expected_product_id.to_string()
                    ),
                    _ => assert!(false),
                }

                match &events[1] {
                    ProductEvent::PriceChanged { id: _, product_id, price } => {
                        assert_eq!(product_id.to_string(), expected_product_id.to_string());
                        assert_eq!(*price, 120);
                    },
                    _ => assert!(false),
                }

                match &events[2] {
                    ProductEvent::TitleChanged { id: _, product_id, title } => {
                        assert_eq!(product_id.to_string(), expected_product_id.to_string());
                        assert_eq!(*title, "Bob".to_string());
                    },
                    _ => assert!(false),
                }
            }
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn get_an_validation_error_if_product_not_empty() {
        let event_result = Product::handle_command(
            &ProductState { id: Uuid::new_v4(), price: 0, title: "".to_string() },
            &ProductCommand::CreateProduct { product_id: Uuid::nil(), price: 100, title: "".to_string() },
        );
        match event_result {
            Err(_error) => assert!(true),
            Ok(_) => assert!(false),
        }
    }

    #[test]
    fn product_created_creates_a_new_product_state() {
        let event_id = "24F017F4-2817-4F76-A95C-3949D909AEF6";
        let product_id = "3c625082-ce7b-4938-aa41-88999937aa64";
        let event = ProductEvent::ProductCreated {
            id: Uuid::parse_str(event_id).unwrap(),
            product_id: Uuid::parse_str(product_id).unwrap(),
        };
        let result = Product::apply(&ProductState { id: Uuid::nil(), price: 0, title: "".to_string() }, &event);
        match result {
            Ok(state) => assert_eq!(state.id.to_string(), product_id),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn change_price_creates_price_changed_event() {
        let product_id = "3c625082-ce7b-4938-aa41-88999937aa64";
        let product = ProductState {
            id: Uuid::new_v4(),
            price: 0,
            title: "".to_string(),
        };
        let command = ProductCommand::ChangePrice{
            product_id: Uuid::parse_str(product_id).unwrap(),
            price: 100,
        };
        let result = Product::handle_command(&product, &command);

        match result {
            Ok(events) => match events[0] {
                ProductEvent::PriceChanged { id: _, product_id: _, price } => assert_eq!(price, 100),
                _ => assert!(false, "Should have been ProductEvent"),
            },
            Err(_) => assert!(false),
        }

    }

    #[test]
    fn applying_the_change_price_event_changes_the_price() {
        let product_id = "3c625082-ce7b-4938-aa41-88999937aa64";
        let product_uuid = Uuid::parse_str(product_id).unwrap();
        let product_state = ProductState {
            id: product_uuid,
            price: 0,
            title: "".to_string(),
        };

        let event = ProductEvent::PriceChanged {
            id: Uuid::nil(),
            product_id: product_uuid,
            price: 42,
        };

        let result = Product::apply(&product_state, &event);

        match result {
            Ok(state) => assert_eq!(state.price, 42),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn change_title_creates_title_changed_event() {
        let product_id = "3c625082-ce7b-4938-aa41-88999937aa64";
        let product = ProductState {
            id: Uuid::new_v4(),
            price: 0,
            title: "".to_string(),
        };

        let expected_title = "Thinking Fast & Slow".to_string();

        let command = ProductCommand::ChangeTitle{
            product_id: Uuid::parse_str(product_id).unwrap(),
            title: expected_title.to_string(),
        };
        let result = Product::handle_command(&product, &command);

        match result {
            Ok(events) => match &events[0] {
                ProductEvent::TitleChanged { id: _, product_id: _, title } => assert_eq!(*title, expected_title),
                _ => assert!(false, "Should have been TitleChanged"),
            },
            Err(_) => assert!(false),
        }

    }

    #[test]
    fn applying_the_change_title_event_changes_the_title() {
        let product_id = "3c625082-ce7b-4938-aa41-88999937aa64";
        let product_uuid = Uuid::parse_str(product_id).unwrap();
        let product_state = ProductState {
            id: product_uuid,
            price: 0,
            title: "".to_string(),
        };

        let expected_title = "The Unicorn Project".to_string();

        let event = ProductEvent::TitleChanged {
            id: Uuid::nil(),
            product_id: product_uuid,
            title: expected_title.to_string(),
        };

        let result = Product::apply(&product_state, &event);

        match result {
            Ok(state) => assert_eq!(state.title, expected_title),
            Err(_) => assert!(false),
        }
    }
}
